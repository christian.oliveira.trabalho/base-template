<?php

require_once 'app/controllers/HomeController.php';

$url = substr($_SERVER["REQUEST_URI"], 1);

if (strpos($url, 'static/') !== false) {
    $file = 'app/views/'.substr($url, 7);

    if (file_exists($file)) {
        if (strpos($url, 'css') !== false) {
            header("Content-type: text/css", true);
        }

        echo file_get_contents($file);
    } else {
        echo '<h1>404</h1>';
    }

    exit;
}

$routes = [
    '' => ['HomeController', 'index'],
];

if (!isset($routes[$url])) {
    echo '<h1>404</h1>';
    exit;
}

[$class, $method] = $routes[$url];
$obj = new $class;
echo $obj->$method();