<?php

class Controller {

    const LAYOUT_PATH = 'app/views/layout/main-layout.html';
    const VIEW_PATH = 'app/views/';

    protected function view(string $view, string $title) {
        $layout = file_get_contents(self::LAYOUT_PATH);
        $view = file_get_contents(self::VIEW_PATH.$view.'.html');

        $page = str_replace('{{Title}}', $title, $layout);
        $page = str_replace('{{content}}', $view, $page);

        return $page;
    }
}